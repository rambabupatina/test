## Test markdown with Direct URL
![Image full URL](https://gitlab.com/rambabupatina/test/-/raw/main/someimage.png)

## Test markdown with full image URL
![Image full URL](https://gitlab.com/rambabupatina/test/-/raw/main/someimage.png)

## Test markdown with relative URL
![Image relative URL](/someimage.png)
